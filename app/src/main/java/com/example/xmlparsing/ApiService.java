package com.example.xmlparsing;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("pizza/")
    Call<Menu>
    getPizza(
      @Query("format") String format);
}
