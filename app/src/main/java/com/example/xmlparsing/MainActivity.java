package com.example.xmlparsing;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getData();

    }


    private void getData()
    {
        WebServiceFactory.getInstance().getPizza("xml").enqueue(new Callback<Menu>() {
            @Override
            public void onResponse(Call<Menu> call, Response<Menu> response) {
                Log.d(TAG,response.body().getItem().get(2).getName());
            }

            @Override
            public void onFailure(Call<Menu> call, Throwable t) {
                Log.d(TAG,t.getLocalizedMessage());
            }
        });
    }

}

