package com.example.xmlparsing;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name="menu")
public class Menu { //https://plugins.jetbrains.com/plugin/8113-pojo-generator

    @ElementList(name="item", required=false, entry="item", inline=true)
    List<Item> item;

    public List<Item> getItem() {return this.item;}
    public void setItem(List<Item> value) {this.item = value;}

    public static class Item {

        @Element(name="cost", required=false)
        String cost;

        @Element(name="name", required=false)
        String name;

        @Element(name="description", required=false)
        String description;

        @Element(name="id", required=false)
        String id;

        public String getCost() {return this.cost;}
        public void setCost(String value) {this.cost = value;}

        public String getName() {return this.name;}
        public void setName(String value) {this.name = value;}

        public String getDescription() {return this.description;}
        public void setDescription(String value) {this.description = value;}

        public String getId() {return this.id;}
        public void setId(String value) {this.id = value;}

    }

}